#!/bin/bash

# jq '.data.currentUser | to_entries | .[] | .key as $key | ( .value |= [ (.nodes[] | {"title": .title}) ] )' ~/tmp/mr-list-gql.json

MILESTONE=${MILESTONE:-"15.10"}


get_mrs(){
    glab api graphql --paginate -f query="$(cat ~/work/weekly-reports/mrs.gql)" -f milestone=${MILESTONE}
}

simplify_gql(){
    jq '[.data.currentUser | to_entries | .[] | .key as $key | ( .value |= [ (.nodes[] | {"title": .title, "project_name": .project.name, "project_path": .project.fullPath, "mulestone": .milestone.title, "state": .state} ) ] ) ] | from_entries'
}