# my-tests

Collection of test scripts I've been using for various Issues and MRs 

## Notes

since scripts do process or produce data that may be sensitive, 2 directories are being ignored by git to avoid accidental leaks:

* `_data`
* `.build`