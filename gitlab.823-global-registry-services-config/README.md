## Test self-signed registry

1. Provision GitLab instance in KinD or other type of k8s cluster
1. Login to GitLab and create sample project (`PROJECT_NAME`) as a "Blank Project"
1. Create Project Access Token
   1. obtain PAT username from project member list (`PAT_USERNAME`)
1. run `testme.sh` to execute the test:
   ```shell
   NAMESPACE="mynamespace" REGISTRY_HOSTNAME="registry.my.foo" PAT_USERNAME="project_99_bot_abcfdfgh123" ./testme.sh
   ```
   providing PAT when prompted for `Password:`
1. Check for image presense in project's container registry

### Requiremets

* kubernetes context should be set to cluster in question
* `NAMESPACE` - set to namespace into which GitLab was deployed
* `REGISTRY_HOSTNAME` - ingress hostname provisioned during deploy for registry service
$ `PROJECT_NAME` - name of the created sample project
* `PAT_USERNAME` - Project Token Username


