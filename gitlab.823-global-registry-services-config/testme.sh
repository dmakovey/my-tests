#!/bin/bash

NAMESPACE=${NAMESPACE:-default}
_my_ip=$(ip route show default | awk '{print $9}')
REGISTRY_HOSTNAME=${REGISTRY_HOSTNAME:-registry.${_my_ip}.nip.io}
PAT_USERNAME=${PAT_USERNAME:?"PAT_USERNAME is required"}
PROJECT_NAME=${PROJECT_NAME:-containerme}

mkdir -p certs/${REGISTRY_HOSTNAME}:5000

kubectl get secret -n ${NAMESPACE} gitlab-wildcard-tls -o json | jq -r '.data["tls.crt"]' | base64 -d > certs/registry-tls.crt

openssl x509 -in certs/registry-tls.crt -text

pushd certs
ln -s ${PWD}/registry-tls.crt ${REGISTRY_HOSTNAME}:5000/ca.crt
ln -s ${REGISTRY_HOSTNAME}:5000 ${REGISTRY_HOSTNAME}:443
popd

podman --cert-dir ${PWD}/certs --username ${PAT_USERNAME} ${REGISTRY_HOSTNAME}

podman pull busybox
podman tag busybox ${REGISTRY_HOSTNAME}/root/containerme:v1.0
podman push --cert-dir=${PWD}/certs ${REGISTRY_HOSTNAME}/root/containerme:v1.0

