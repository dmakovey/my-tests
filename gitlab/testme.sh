#!/bin/bash
source scripts/ci/autodevops.sh 
./scripts/ci/install_spec_dependencies 
helm version
kubectl version 
set_context
   
export RUBYGEMS_VERSION="3.4" \
GO_VERSION="1.19" \
RUST_VERSION="1.65" \
PG_VERSION="13" \
CHROME_VERSION="109" \
RSPEC_TAGS="~type:feature"

./scripts/ci/integration_spec_setup

bundle config set --local path 'gems'
bundle config set --local frozen 'true'
bundle install -j $(nproc)

testme_rspec(){
  local filepath=${1:-spec}
  bundle exec rspec -c -f d ${filepath} -t "${RSPEC_TAGS}"
}

testme_rubocop(){
  bundle exec rubocop ${1}
}