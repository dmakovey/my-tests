#!/bin/bash -x

# Set 
#  BASE_DOMAIN - to override domain

GL_FLOW=${HOME}/work/python/gl-workflow/scripts/gl-flow.py
GLAB=${GLAB:-"glab"}
CLUSTER_NAME=${CLUSTER_NAME:-"testme"}
BRANCH_NAME="cluster/${CLUSTER_NAME}"
BRANCH_NAME_URL=$(python -c "import urllib.parse as p; print(p.quote_plus('''$BRANCH_NAME'''))")
PROJECT_ID="33422111"
PROJECT="gitlab-org/distribution/infrastructure/openshift-provisioning"
SA_KEYFILE=${SA_KEYFILE:-"${HOME}/secure/unlocked/gitlab/ocp/hive-operator-sa-key-dmakovey.json"}
SSH_PUBKEY_FILE=${SSH_PUBKEY_FILE:-${HOME}/.ssh/gitlab_rsa.pub}
ZONE_ID=${ZONE_ID:-"openshift-hive-gitlab"}
BASE_BRANCH=${BASE_BRANCH:-"main"}
CLUSTER_VERSION=${CLUSTER_VERSION:-"4.10.18"}

create(){
    ${GL_FLOW} api --method post --query-data '{"branch": "'"${BRANCH_NAME}"'", "ref": "'"${BASE_BRANCH}"'"}' /projects/${PROJECT_ID}/repository/branches
}

create2(){
    ${GLAB} api --X POST --query-data '{"branch": "'"${BRANCH_NAME}"'", "ref": "'"${BASE_BRANCH}"'"}' /projects/${PROJECT_ID}/repository/branches
}


launch_old(){
    SA_KEY=$(cat ${SA_KEYFILE} | jq -R .)
    SSH_PUBKEY=$(cat ${SSH_PUBKEY_FILE} | jq -R .)
    if [ -n "${BASE_DOMAIN}" ]
    then
        JQ_DOMAIN_VAR=", {\"key\": \"BASE_DOMAIN\", \"value\": \"${BASE_DOMAIN}\"}"
    else
        JQ_DOMAIN_VAR=""
    fi
    if [ -z "${PREVIOUS_DEPLOY_JOB_ID}" ]
    then
        JQ_CMD='{"ref": $bn, "variables": [{"key": "ZONE_ID", "value": $zone_id}, {"key": "CLUSTER_VERSION", "value": $cv}, {"key": "GOOGLE_APPLICATION_CREDENTIALS", "variable_type": "file", "value": $sa}, {"key": "SSH_PUBLIC_KEY_FILE", "variable_type": "file", "value": $ssh}'"${JQ_DOMAIN_VAR}"']}'
        JQ_ARGS="--arg zone_id ${ZONE_ID} --arg cv ${CLUSTER_VERSION}"
    else
        JQ_CMD='{"ref": $bn, "variables": [{"key": "ZONE_ID", "value": $zone_id}, {"key": "CLUSTER_VERSION", "value": $cv}, {"key": "GOOGLE_APPLICATION_CREDENTIALS", "variable_type": "file", "value": $sa}, {"key": "SSH_PUBLIC_KEY_FILE", "variable_type": "file", "value": $ssh}'"${JQ_DOMAIN_VAR}"', {"key": "PREVIOUS_DEPLOY_JOB_ID", "value": $ji}]}'
        JQ_ARGS="--arg ji ${PREVIOUS_DEPLOY_JOB_ID} --arg zone_id ${ZONE_ID} --arg cv ${CLUSTER_VERSION}"
    fi
    # VARS=$(cat vars.txt)
    POST_DATA=$(echo "[]" | jq --rawfile sa "${SA_KEYFILE}" --rawfile ssh "${SSH_PUBKEY_FILE}" --arg bn "${BRANCH_NAME}" ${JQ_ARGS} \
        "${JQ_CMD}" )
    ${GL_FLOW} api --method post --post-data "${POST_DATA}" /projects/${PROJECT_ID}/pipeline
}

launch(){
    SA_KEY=$(cat ${SA_KEYFILE} | jq -R .)
    SSH_PUBKEY=$(cat ${SSH_PUBKEY_FILE} | jq -R .)
    BASE_VARIABLES=("ZONE_ID:$ZONE_ID" \
                    "CLUSTER_VERSION:$CLUSTER_VERSION" \
    )
    PIPELINE_VARS=${PIPELINE_VARS:-""}
    for bv in "${BASE_VARIABLES[@]}"
    do
        PIPELINE_VARS="${PIPELINE_VARS} --variables-env $bv"
    done
    BASE_VARIABLES_FILES=( \
                    "GOOGLE_APPLICATION_CREDENTIALS:${SA_KEYFILE}" \
                    "SSH_PUBLIC_KEY_FILE:${SSH_PUBKEY_FILE}" \
    )
    for bv in "${BASE_VARIABLES_FILES[@]}"
    do
        PIPELINE_VARS="${PIPELINE_VARS} --variables-file $bv"
    done
    if [ -n "${BASE_DOMAIN}" ]
    then
        PIPELINE_VARS="${PIPELINE_VARS} --variables-env BASE_DOMAIN:${BASE_DOMAIN}"
    fi
    if [ -n "${PREVIOUS_DEPLOY_JOB_ID}" ]
    then
        PIPELINE_VARS="${PIPELINE_VARS} --variables-env PREVIOUS_DEPLOY_JOB_ID:${PREVIOUS_DEPLOY_JOB_ID}"
    fi
    ${GLAB} ci -R ${PROJECT} run -b ${BRANCH_NAME} ${PIPELINE_VARS}
}


rebase(){
    local current_ref=$(git branch --show-current)
    git pull
    git checkout ${BRANCH_NAME}
    git pull
    git rebase -i origin/${current_ref}
    git push --force-with-lease
    git checkout ${current_ref}
}

for cmd in $@
do
    $cmd
done
