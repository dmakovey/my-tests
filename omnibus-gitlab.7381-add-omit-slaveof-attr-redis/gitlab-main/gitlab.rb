external_url 'http://localhost'

roles ['redis_sentinel_role', 'redis_replica_role', 'postgres_role', 'application_role', 'consul_role']
redis['bind'] = '0.0.0.0'
redis['port'] = 6379
redis['password'] = 'redis-password-goes-here'
redis['master_password'] = 'redis-password-goes-here'
redis['master_ip'] = '10.128.0.57' # IP of primary Redis server
redis['master_name'] = 'gitlab-redis' # must be the same in every sentinel node
sentinel['bind'] = '0.0.0.0'
sentinel['port'] = 26379 # uncomment to change default port
sentinel['quorum'] = 2

gitlab_rails['redis_sentinels'] = [
  {'host' => '10.128.0.56', 'port' => 26379},
  {'host' => '10.128.0.57', 'port' => 26379},
  {'host' => '10.128.0.58', 'port' => 26379}
]
