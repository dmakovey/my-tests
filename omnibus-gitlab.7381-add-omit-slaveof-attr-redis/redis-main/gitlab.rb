roles ['redis_sentinel_role', 'redis_master_role']
gitlab_rails['auto_migrate'] = false
redis['bind'] = '0.0.0.0'
redis['port'] = 6379
redis['password'] = 'redis-password-goes-here'
redis['master_name'] = 'gitlab-redis' # must be the same in every sentinel node
redis['master_password'] = 'redis-password-goes-here' # the same value defined in redis['password'] in the primary instance
redis['master_ip'] = '10.128.0.57' # ip of the initial primary redis instance
sentinel['bind'] = '0.0.0.0'
sentinel['port'] = 26379 # uncomment to change default port
sentinel['quorum'] = 2
