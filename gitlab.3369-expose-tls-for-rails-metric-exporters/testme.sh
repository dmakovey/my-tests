#!/bin/bash

# Based on:
# https://rob-blackbourn.medium.com/how-to-use-cfssl-to-create-self-signed-certificates-d55f76ba5781

KIND_CLUSTER_NAME=${KIND_CLUSTER_NAME:-railstls}
KIND_CLUSTER_VERSION=${KIND_CLUSTER_VERSION:-1.22.2}
NAMESPACE=${NAMESPACE:-tls}
LOCAL_IP=${LOCAL_IP:-192.168.3.194}
CHART_DIR=${CHART_DIR:-.}

MY_PATH=$(realpath $0)
MY_DIR=${MY_PATH%/*}
TLS_DIR=${MY_DIR}/TLS
CHART_DIR=$(realpath ${CHART_DIR})

generate_ca(){
    pushd ${TLS_DIR}
    cfssl gencert -initca config/cfssl-ca.json | cfssljson -bare ca
    popd
}

generate_intermediate_ca(){
    pushd ${TLS_DIR}
    cfssl gencert -initca config/cfssl-intermediate-ca.json | cfssljson -bare intermediate_ca
    cfssl sign -ca ca.pem -ca-key ca-key.pem -config config/cfssl-profile.json -profile intermediate_ca intermediate_ca.csr | cfssljson -bare intermediate_ca
    popd
}

_generate_cert(){
    local profile=${1:-server}
    pushd ${TLS_DIR}
    cfssl gencert -ca intermediate_ca.pem -ca-key intermediate_ca-key.pem -config config/cfssl-profile.json -profile=${profile} config/cfssl-host.json | cfssljson -bare host-${profile}
    popd
}

generate_cert_server(){
    _generate_cert server
}

generate_cert_peer(){
    _generate_cert peer
}

generate_cert_client(){
    _generate_cert client
}

create_kind_cluster(){
    pushd ${CHART_DIR}
    kind create cluster --config examples/kind/kind-ssl.yaml --image kindest/node:v${KIND_CLUSTER_VERSION} --name ${KIND_CLUSTER_NAME}
    popd
}

create_namespace(){
    kubectl create namespace ${NAMESPACE}
    kubectl config set-context --current --namespace=${NAMESPACE}
}

generate_cert_puma(){
    pushd ${TLS_DIR}
    NAMESPACE=$NAMESPACE bash -x ../../gitlab.sh-support-puma-ssl/testme.sh create_cert
    popd
}

create_secrets(){
    # kubectl create secret generic --namespace=${NAMESPACE} metrics.gitlab.tls-ca --from-file=metrics.gitlab.tls-ca=TLS/ca.pem
    kubectl create secret generic --namespace=${NAMESPACE} metrics.gitlab.tls-ca --from-file=metrics.gitlab.tls-ca=${TLS_DIR}/intermediate_ca.pem
    kubectl create secret tls gitlab-webservice-metrics-tls --namespace=${NAMESPACE} --cert=${TLS_DIR}/host-server.pem --key=${TLS_DIR}/host-server-key.pem
    kubectl create secret tls gitlab-sidekiq-metrics-tls --namespace=${NAMESPACE} --cert=${TLS_DIR}/host-server.pem --key=${TLS_DIR}/host-server-key.pem
    kubectl create secret tls -n ${NAMESPACE} gitlab-webservice-tls --cert=${TLS_DIR}/puma.crt --key=${TLS_DIR}/puma.key
}

deploy_gitlab(){
    pushd ${CHART_DIR}
    helm upgrade --install gitlab . --timeout 600s \
        --set global.hosts.domain=${LOCAL_IP}.nip.io \
        --set prometheus.install=true \
        --set global.image.pullPolicy=Always \
        --set certmanager-issuer.email=dmakovey@gitlab.com \
        --set gitlab.sidekiq.metrics.enabled=true \
        --set gitlab.sidekiq.metrics.tls.enabled=true \
        --set gitlab.webservice.metrics.tls.enabled=true \
        --set gitlab.webservice.tls.enabled=true \
        -f examples/kind/values-base.yaml \
        -f examples/kind/values-ssl.yaml \
        -f ${TLS_DIR}/prometheus-values-tls.yaml \
        --debug
    popd
}


_toolbox(){
    kubectl get pods --namespace=${NAMESPACE} -o json -l app=toolbox | jq -r '.items[0].metadata.name'
}

check_prometheus_metadata(){
    kubectl exec $(_toolbox) -- curl -s -L http://gitlab-prometheus-server/api/v1/metadata | jq -r '.data | to_entries | .[] | .key'
}

check_webservice_metrics(){
    # kubectl exec $(_toolbox) -- curl --insecure --verbose  https://gitlab-webservice-default:8083/metrics | grep -ve '^#' | sed -e 's/{.*$//g' | sort -u
    kubectl exec $(_toolbox) -- curl -s --insecure https://gitlab-webservice-default:8083/metrics | grep -ve '^#' | sed -e 's/{.*$//g' | awk '{print $1;}' | sort -u

}
check_targets(){
    kubectl exec $(_toolbox) -- curl -s -L "http://gitlab-prometheus-server/api/v1/targets?state=active" | jq -r '.data.activeTargets | to_entries | [.[] | .value | { "key": (.labels.app // .scrapeUrl), "value": { "url": .scrapeUrl, "error": .lastError}}] | from_entries'
}

build(){
    generate_ca
    generate_intermediate_ca
    generate_cert_server
    generate_cert_puma
}

deploy(){
    create_kind_cluster
    create_namespace
    create_secrets
    deploy_gitlab
}

check(){
    echo "==> Metrics produced by webservice and consumed by prometheus:"
    comm -1 -2 <(check_prometheus_metadata | sort) <(check_webservice_metrics | sort)
    echo "==> Un-scraped metrics"
    comm -1 -3 <(check_prometheus_metadata | sort) <(check_webservice_metrics | sort)
    echo "==> Prometheus targets"
    check_targets
}

for cmd in "$@"
do
    $cmd
done