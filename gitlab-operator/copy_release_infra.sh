#!/bin/bash -x


_suffix=${1:+"/.build"}
SRC_DIR=${1:-"${CERTIFICATION_BASE_DIR}"}${_suffix}

if [ -z "${SRC_DIR}" ]; then
  echo "Source directory is not specified"
  exit 1
fi

mkdir -p .build

cp -rL ${SRC_DIR}/cluster .build/
cp -L ${SRC_DIR}/gl-distribution-oc* .build/
