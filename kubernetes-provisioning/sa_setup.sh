#!/bin/bash

GCLOUD_CONFIG_NAME=${GCLOUD_CONFIG_NAME:-${USER}}
GCLOUD_PROJECT=${GCLOUD_PROJECT:-"dmakovey-c2a477b8"}
GCLOUD_ACCOUNT=${GCLOUD_ACCOUNT:-"dmakovey@gitlab.com"}

SA_NAME=${SA_NAME:-"k8sprovisioner"}
SA_KEY_FILE=${SA_KEY_FILE:-"gcloud.json"}

SERVICE_ACCOUNT_ID=${SERVICE_ACCOUNT_ID:-"${SA_NAME}@${GCLOUD_PROJECT}.iam.gserviceaccount.com"}

gcloud_create_config(){
  gcloud config configurations create ${GCLOUD_CONFIG_NAME}
  gcloud config set project ${GCLOUD_PROJECT}
  gcloud config set account ${GCLOUD_ACCOUNT}
}

gcloud_auth(){
  gcloud auth login
}

create_sa(){
  gcloud iam service-accounts create ${SA_NAME} \
    --configuration=${GCLOUD_CONFIG_NAME} \
    --description="Kubernetes cluster provisioner" \
    --display-name="Kubernetes Provisioner account"
  SERVICE_ACCOUNT_ID=$(gcloud iam service-accounts list --configuration=${GCLOUD_CONFIG_NAME} --filter=${SA_NAME} --project=${GCLOUD_PROJECT} --format=json | jq -r '.[].email')
}

create_key(){
  gcloud iam service-accounts keys create ${SA_KEY_FILE} --configuration=${GCLOUD_CONFIG_NAME} --iam-account=${SERVICE_ACCOUNT_ID}
}

assign_service_account_roles(){
  local iam_roles=( "roles/iam.securityAdmin" "roles/iam.serviceAccountAdmin" "roles/iam.serviceAccountUser" "roles/iam.serviceAccountKeyAdmin" )
  local p_roles=( "roles/dns.admin" "roles/storage.admin" "roles/compute.admin" "roles/container.admin" "roles/container.clusterAdmin" "roles/iam.serviceAccountUser" )

  for role in ${iam_roles[@]}
  do
    gcloud iam service-accounts add-iam-policy-binding \
            "${SERVICE_ACCOUNT_ID}" \
            --configuration=${GCLOUD_CONFIG_NAME} \
            --member="serviceAccount:${SERVICE_ACCOUNT_ID}" \
            --project=${GCLOUD_PROJECT} \
            --role=${role}
  done
  for role in ${p_roles[@]}
  do
    gcloud projects add-iam-policy-binding ${GCLOUD_PROJECT} \
            --configuration=${GCLOUD_CONFIG_NAME} \
            --member="serviceAccount:${SERVICE_ACCOUNT_ID}" \
            --role=${role}
  done
}

all_in_one(){
  gcloud_create_config
  gcloud_auth
  gcloud iam service-accounts list
  create_sa
  create_key
  assign_service_account_roles
}

setup_sa(){
  create_sa
  create_key
  assign_service_account_roles
}

if [ $# -lt 1 ]; then
  all_in_one
else
  for cmd in "$@"
  do
    $cmd
  done
fi
