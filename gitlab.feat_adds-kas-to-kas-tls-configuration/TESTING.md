https://docs.gitlab.com/charts/advanced/internal-tls/#generating-certificates-for-internal-use

```shell
xclip -o > gitlab-internal-tls.sh
chmod +x gitlab-internal-tls.sh

export NAMESPACE="gitlab"
export RELEASE="gitlab"
export KIND_CLUSTER_NAME="kastls"
```

```shell
cp -r ~/work/my-tests/gitlab.3369-expose-tls-for-rails-metric-exporters/* .
```

```shell
./testme.sh create_kind_cluster
./testme.sh create_namespace
```

```shell
./gitlab-internal-tls.sh
```

```
2022/10/12 07:56:19 [INFO] generating a new CA key and certificate from CSR
2022/10/12 07:56:19 [INFO] generate received request
2022/10/12 07:56:19 [INFO] received CSR
2022/10/12 07:56:19 [INFO] generating key: ecdsa-256
2022/10/12 07:56:19 [INFO] encoded CSR
2022/10/12 07:56:19 [INFO] signed certificate with serial number 640567876323520335562395044037230686424567624873
2022/10/12 07:56:19 [INFO] generate received request
2022/10/12 07:56:19 [INFO] received CSR
2022/10/12 07:56:19 [INFO] generating key: ecdsa-256
2022/10/12 07:56:19 [INFO] encoded CSR
2022/10/12 07:56:19 [INFO] signed certificate with serial number 150184739901197990591973752641187803415037878152
secret/gitlab-internal-tls created
secret/gitlab-internal-tls-ca created
```

```shell
helm upgrade --install "${RELEASE}" --namespace "${NAMESPACE}" -f kas-values.yaml .
```

```
Release "gitlab" does not exist. Installing it now.
NAME: gitlab
LAST DEPLOYED: Wed Oct 12 07:59:09 2022
NAMESPACE: gitlab
STATUS: deployed
REVISION: 1
NOTES:
=== NOTICE
The minimum required version of PostgreSQL is now 12. See https://gitlab.com/gitlab-org/charts/gitlab/-/blob/master/doc/installation/upgrade.md for more details.

=== NOTICE
You've installed GitLab Runner without the ability to use 'docker in docker'.
The GitLab Runner chart (gitlab/gitlab-runner) is deployed without the `privileged` flag by default for security purposes. This can be changed by setting `gitlab-runner.runners.privileged` to `true`. Before doing so, please read the GitLab Runner chart's documentation on why we
chose not to enable this by default. See https://docs.gitlab.com/runner/install/kubernetes.html#running-docker-in-docker-containers-with-gitlab-runners

=== NOTICE
The in-chart NGINX Ingress Controller has the following requirements:
    - Kubernetes version must be 1.19 or newer.
    - Ingress objects must be in group/version `networking.k8s.io/v1`.
```

```shell
kubectl get cm gitlab-kas -oyaml | yq '.data["config.yaml"]' | yq .private_api
```

```yaml
listen:
  address: :8155
  authentication_secret_file: /etc/kas/.gitlab_kas_private_api_secret
  ca_certificate_file: /etc/ssl/certs/ca-certificates.crt
  certificate_file: /etc/kas/tls.crt
  key_file: /etc/kas/tls.key
```

```shell
helm upgrade --install "${RELEASE}" --namespace "${NAMESPACE}" -f kas-values.yaml .
```

can't reach `gitlab.192.168.3.194` on both `http` and `https`.


modified `gitlab-internal-tls.sh`:

```shell
CERT_SANS="*.${NAMESPACE}.svc,${RELEASE}-metrics.${NAMESPACE}.svc,*.${RELEASE}-gitaly.${NAMESPACE}.svc,private-api.test"
```

```shell
./testme.sh create_kind_cluster
./testme.sh create_namespace
./gitlab-internal-tls.sh
```

confirmed secrets properly created:

```shell
kubectl get secrets
```

```shell
helm upgrade --install "${RELEASE}" --namespace "${NAMESPACE}" -f kas-values.yaml .
```

```shell
helm upgrade --install "${RELEASE}" --namespace "${NAMESPACE}" -f examples/kind/values-base.yaml -f examples/kind/values-ssl.yaml -f kas-values.yaml .
```