#!/bin/bash

export GITLAB_TOKEN=${GITLAB_TOKEN}
export GITLAB_HOST=${GITLAB_HOST}

FROM_DATE=${FROM_DATE:-"2 days ago"}
_updated_after=$(date --date "${FROM_DATE}" '+%Y-%m-%dT00:00:00Z')}

PROJECT_PATH=${PROJECT_PATH:-gitlab/omnibus-gitlab}
PROJECT_ID=${PROJECT_ID:-283}

WORKDIR=${WORKDIR:-$(mktemp -d pipelines-XXXXX)}
mkdir -p ${WORKDIR}

glab api -X GET -f "updated_after=${_updated_after}" --paginate /projects/${PROJECT_ID}/pipelines \
  | jq -n '[inputs|.[]]' > ${WORKDIR}/pipelines_list.json

for pid in $(jq -r '.[].id' ${WORKDIR}/pipelines_list.json)
do
  glab api -X GET /projects/${PROJECT_ID}/pipelines/${pid}/bridges \
    > ${WORKDIR}/triggers-${pid}.json
done

for tpid in $(for t in ${WORKDIR}/triggers-*.json; do jq -r '.[] | select(.downstream_pipeline.status != "success" and .downstream_pipeline != null) | .downstream_pipeline.id' $t; done)
do
  glab ci get -R ${PROJECT_PATH} -p ${tpid} -F json \
    > ${WORKDIR}/triggered-${tpid}.json
done

for t in ${WORKDIR}/triggered-*.json
do
  jq -r '.id as $pid | .ref as $ref | .web_url as $url | .jobs[] | select(.status == "failed") | "* [\($ref)](\($url)) [\(.name)](\(.web_url))"' $t
done 
