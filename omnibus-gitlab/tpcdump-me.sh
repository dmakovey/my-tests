#!/bin/bash -x

# docker run -e ASSET_PATH=/gitlab-assets -v ${PWD}:/omnibus-gitlab -v ${HOME}/gitlab-assets:/gitlab-assets -it registry.gitlab.com/gitlab-org/gitlab-omnibus-builder/debian_11_arm64:4.24.0 -w /omnibus-gitlab bash -c 'bundle install && bundle binstubs --all && bundle exec rake build:project'

timestamp=$(date '+%Y%m%d%H%M%S')
MY_ADDR=${MY_ADDR:-"10.128.0.60"}
# touch ${HOME}/omnibus-${timestamp}.dump
sudo tcpdump -w ${HOME}/omnibus.dump -s 256 -G 300 -Z root &
sleep 2
TCPDUMP_PIDS=$(ps --ppid $! -o pid=)
# docker run --rm \
#   -e COMPILE_ASSETS=true \
#   -v ${PWD}:/omnibus-gitlab \
#   -v ${HOME}/gitlab-assets:/gitlab-assets \
#   -w /omnibus-gitlab \
#   -it \
#   registry.gitlab.com/gitlab-org/gitlab-omnibus-builder/debian_11_arm64:4.24.0 \
#   bash -c 'bundle install && bundle binstubs --all && bundle exec rake build:project' > ${HOME}/omnibus.log 2>&1 || SAVE_DUMP="yes"
container_id=$(docker create \
  -e COMPILE_ASSETS=true \
  -v ${PWD}:/omnibus-gitlab \
  -v ${HOME}/gitlab-assets:/gitlab-assets \
  -w /omnibus-gitlab \
  --name omnibus-builder \
  registry.gitlab.com/gitlab-org/gitlab-omnibus-builder/debian_11_arm64:4.24.0 \
  bash -c '( exec 2>&1 ; bundle install && bundle binstubs --all && bundle exec rake build:project ) | tee /tmp/bundle.log | grep -m 1 -F "Connection reset by peer" && cat /tmp/bundle.log && exit 1' )
docker start -ai omnibus-builder || SAVE_DUMP="yes"
sudo kill ${TCPDUMP_PIDS}
if [ "${SAVE_DUMP}" = "yes" ] ; then
  docker logs omnibus-builder > ${HOME}/omnibus-${timestamp}.log
  # mv ${HOME}/omnibus.log ${HOME}/omnibus-${timestamp}.log
  mv ${HOME}/omnibus.dump ${HOME}/omnibus-${timestamp}.dump
  sleep 20
fi
docker rm ${container_id}
