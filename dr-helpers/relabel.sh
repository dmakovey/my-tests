#!/bin/sh
# Input: list of Issue URLs
# "- http://foo.bar/issues/-/1+"

ISSUE_LIST=${1:-list.url}

for i in $(sed -e 's/^\- \(.*\)+/\1/g' ${ISSUE_LIST})
do
        glab issue update --label "workflow::verification" --label "FedRAMP DR Status::Open" "$i"
done
