#!/bin/sh
# Input: list of Issue URLs
# "- http://foo.bar/issues/-/1+"

ISSUE_LIST=${1:-list.url}
CVE_SCORE=${2:-Medium}
DATE_DETECTED=${3:-2022-11-10}

for i in $(sed -e 's/^\- \(.*\)+/\1/g' ${ISSUE_LIST})
do
        title=$(glab issue view -j "$i" | jq -r .title | sed 's#detected ##g')
        cve=$(echo "$title"| awk '{print $1}')
        description=$(glab issue view -j "$i" | jq -r .description)
        details=$(echo "$description" | grep -aF "$cve" | sed -e 's/^.* found in `\(.*\)` `for package.*` binary `\(.*\)`.*$/\1 in \2/g')
        container=$(echo "$details" | sed 's/^\(.*\) in .*$/\1/g')
        digest=$(skopeo inspect docker://"${container}" | jq -r .Digest)
        binaries=$(echo "$details" | sed 's/^.* in \(.*\)$/\1/g')
        echo "| $cve in $binaries | $container $digest | ${CVE_SCORE} | ${DATE_DETECTED} |"
done
