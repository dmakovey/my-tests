Helper scripts to file DRs:

* `dr_vuln_table.sh` - render DR vulnerability report table
* `relabel.sh` - relabel related issues according to DR requirements
