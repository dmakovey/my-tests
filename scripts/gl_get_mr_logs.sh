#!/bin/bash -x

GL_FLOW=${GL_FLOW:-"${HOME}/work/python/gl-workflow/scripts/gl-flow.py"}

MR=${MR:-"1425"}
GIT_REF=${GIT_REF:-"refs/merge-requests/${MR}/merge"}

PROJECT_ID=${PROJECT_ID:-4359271}

OUTPUT_DIR=${OUTPUT_DIR:-${PWD}/_data}

mkdir -p ${OUTPUT_DIR}

PIPELINES_JSON=${PIPELINES_JSON:-${OUTPUT_DIR}/${MR}.json}
if [ -r ${PIPELINES_JSON} -a -n "${USE_CACHE}" ]; then
	echo "Using cached pipeline list"
else
	${GL_FLOW} pipelines --brief --git-ref=${GIT_REF} ${PROJECT_ID} >${PIPELINES_JSON}
fi

# for req in $(jq -r '.[] | "/projects/\(.project_id)/pipelines/\(.id)/jobs"' ${PIPELINES_JSON})
# do
#     ${GL_FLOW} api --query-data '{"include_retried": true}' ${req}
# done | tee ${PIPELINES_JOBS_JSON}

mkdir -p ${OUTPUT_DIR}/${PROJECT_ID}/traces

for req in $(jq -r '.[] | "/projects/\(.project_id)/pipelines/\(.id)/jobs"' ${PIPELINES_JSON}); do
	tgt="$(echo $req | sed 's#/projects/##;s#/pipelines##;s#/jobs##').json"
	[ -e "${OUTPUT_DIR}/${tgt}" ] || glab api --paginate -X GET -F include_retried=true ${req} >${OUTPUT_DIR}/${tgt}
done

for ppl in ${OUTPUT_DIR}/${PROJECT_ID}/*.json; do
	JOB_FILTER=${JOB_FILTER:-"true"}
	for trace in $(jq ".[] | select( ${JOB_FILTER} )" $ppl |
		jq -r '"/projects/\(.pipeline.project_id)/jobs/\(.id)/trace"'); do
		job_id=$(echo $trace | sed -e 's#/projects/4359271/jobs/##; s#/trace##')
		_trace_file=${OUTPUT_DIR}/${PROJECT_ID}/traces/${job_id}.log
		[ -e "${_trace_file}" ] || glab api ${trace} >${_trace_file}
	done
done
