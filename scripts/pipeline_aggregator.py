#!/bin/env python

# input: pipeline JSON files including jobs
#
# Generating JSON files:
#   glab api -X GET --paginate -F updated_after="2023-10-01" /projects/gitlab%2Fcharts%2Fcomponents%2Fimages/pipelines \
#     | jq -n '[inputs|.[]]' | jq '[.[]|select(.name=="AUTO_DEPLOY_BUILD_PIPELINE")]' \
#     > ~/tmp/pipeline-times2/pipelines.json
#   
#   for p in $(jq -r '.[].id' ~/tmp/pipeline-times2/pipelines.json)
#   do
#     glab ci get -R gitlab/charts/components/images -p "${p}" -F json \
#       | jq '{"ref": .ref, "duration": .duration, "id": .id, "started_at": .started_at, "jobs": [.jobs[] | {"name": .name, "duration": .duration}]}' \
#       > ~/tmp/pipeline-times2/${p}.json
#   done
#
# output: JSON/CSV outlining job execution times 

import json
import sys
import argparse
from csv import DictWriter
from datetime import datetime
import traceback
import numpy
import matplotlib.pyplot as plt

from copy import deepcopy

def load_files(files, after, before):
    after_t=None
    before_t=None
    if after:
        after_t=datetime.strptime(after,'%Y-%m-%d')
    if before:
        before_t=datetime.strptime(before,'%Y-%m-%d')
    pipelines={}
    for fname in files:
        short_name=fname.split('/')[-1].rsplit('.',2)[-2]
        with open(fname) as f:
            data=json.loads(f.read())
            if not data['started_at']:
                continue
            # started_at_t=datetime.strptime(data['started_at'],'%Y-%m-%dT%H:%M:%S.%fZ')
            try:
                started_at_t=datetime.strptime(data['started_at'],'%Y-%m-%dT%H:%M:%S.%fZ')
            except:
                started_at_t=datetime.strptime(data['started_at'],'%Y-%m-%dT%H:%M:%SZ')
            if after_t:
                if started_at_t < after_t:
                    continue
            if before_t:
                if started_at_t > before_t:
                    continue
            pipelines[short_name]=data
    return pipelines

def reduce_pipelines(pipelines):
    pipeline_attributes=('id', 'ref', 'duration', 'started_at')
    job_attributes=('name','duration')
    reduced={}
    for pname in pipelines:
        p=pipelines[pname]
        reduced[pname]={}
        for pattr in pipeline_attributes:
            reduced[pname][pattr]=p[pattr]
        reduced[pname]["jobs"]=[]
        for job in pipelines[pname]["jobs"]:
            reduced_job={}
            for jattr in job_attributes:
                reduced_job[jattr]=job[jattr]
            reduced[pname]['jobs'].append(reduced_job)
    return reduced

def _reduce_to_jobs(pipeline, jobs_total=False):
    """Accepts reduced pipeline. """
    pivoted_jobs={}
    jt=0
    for j in pipeline['jobs']:
        pivoted_jobs[j['name']]=j['duration']
        jt+=j['duration']
    if jobs_total:
        pivoted_jobs['jobs_duration']=jt
    return pivoted_jobs

def reduce_to_jobs(pipelines, pipeline_duration=False, jobs_total=False):
    """Accepts pipeline dict. Reduce pipelines to a dictionary of 
    {
      "pipeline1" : {
         "job1": <duration1>,
         ...
      },
      ...
    }
    """
    reduced_pipelines={}
    for pname in pipelines:
        reduced_pipelines[pname]=_reduce_to_jobs(pipelines[pname], jobs_total=jobs_total)
        if pipeline_duration:
            reduced_pipelines[pname]['pipeline_duration']=pipelines[pname]['duration']
    return reduced_pipelines

def normalize_jobs(reduced_pipelines):
    """make sure all pipelines have the same jobs listed. Add "missing" jobs if needed """
    all_jobs=set()
    normalized_pipelines={}
    for pname in reduced_pipelines:
        p=reduced_pipelines[pname]
        all_jobs.update(p.keys())
    for pname in reduced_pipelines:
        p=reduced_pipelines[pname]
        normalized={}
        for jname in all_jobs:
            if not jname in p:
                normalized[jname]=None
            else:
                normalized[jname]=p[jname]
        normalized_pipelines[pname]=normalized
    return (normalized_pipelines, all_jobs)

def extract_delta(normalized_pipelines):
    keys=list(normalized_pipelines.keys())
    keys.sort()
    prev=None
    deltas={}
    for k in keys:
        deltas[k]={}
        if prev:
            for jname in normalized_pipelines[k]:
                deltas[k][jname]=round((normalized_pipelines[k][jname] or 0) - (normalized_pipelines[prev][jname] or 0), 2)
        else:
            for jname in normalized_pipelines[k]:
                deltas[k][jname]=0
        prev=k
    return deltas

def plot(npipelines, poly_n=1, values=False, connect_values=False, suffix='', trend_line=True):
    pipelines=list(npipelines.keys())
    pipelines.sort()
    x=[int(i) for i in pipelines]
    x.sort()
    xp=numpy.linspace(x[0],x[-1],(x[-1]-x[0]) % 3)

    for j in all_jobs:
        # print(j, end=' ')
        y=[]
        for p in x:
            y.append(npipelines[str(p)][j])
        try:
            if trend_line:
                p=numpy.polyfit(x,y,poly_n)
                # print(numpy.polyval(p,306175), numpy.polyval(p,int(pipelines[-1])), sep=', ')
                plt.plot(x,numpy.polyval(p,x),'-',label=j+suffix+'_trend')
            if values:
                plt.plot(x,y, '-' if connect_values else '.',label=j+suffix)
            plt.legend()
            # plt.legend(bbox_to_anchor=(100, 1), borderaxespad=0)
        except Exception as e:
            # print("  FAILED")
            pass
    # plt.show()

def _split(l,n):
    if n == 1:
        return [l]
    chunk_len=len(l) // n 
    i=1
    chunks=[]
    while i<n:
        chunks.append(l[(i-1)*chunk_len:i*chunk_len])
        i=i+1
    chunks.append(l[(i-1)*chunk_len:])
    return chunks

# Denoise ideas from https://stackoverflow.com/questions/37598986/reducing-noise-on-data
def denoise_savgol(npipelines, denoise_param=55):
    """requires all_jobs (i.e. normalized_jobs)"""
    from scipy.signal import savgol_filter
    rpipelines=deepcopy(npipelines)
    for j in all_jobs:
        y=[]
        pkeys=rpipelines.keys()
        # first pass: collect job's runtime
        for p in pkeys:
            y.append(rpipelines[p][j])
        # second pass: denoise values  
        yy=savgol_filter(y, denoise_param, 2)
        # third pass: re-inject values back
        for p,_y in zip(pkeys,yy):
            rpipelines[p][j]=_y
    return rpipelines

def denoise_lfilter(npipelines, denoise_param=15):
    """requires all_jobs (i.e. normalized_jobs)"""
    from scipy.signal import lfilter
    rpipelines=deepcopy(npipelines)
    for j in all_jobs:
        y=[]
        pkeys=rpipelines.keys()
        # first pass: collect job's runtime
        for p in pkeys:
            y.append(rpipelines[p][j])
        # second pass: denoise values  
        n=denoise_param
        b=[1.0 / n] * n
        a=1
        try:
            yy=lfilter(b,a,y)
        except TypeError as te:
            print("Can't denoise ",j)
            continue
        # third pass: re-inject values back
        for p,_y in zip(pkeys,yy):
            rpipelines[p][j]=_y
    return rpipelines

def to_percentage(npipelines):
    """requires all_jobs (i.e. normalized_jobs)"""
    rpipelines=deepcopy(npipelines)
    _all_jobs=all_jobs.copy()
    for j in _all_jobs:
        y=[]
        pkeys=rpipelines.keys()
        for p in pkeys:
            y.append(rpipelines[p][j])
        ny=numpy.array(y)
        try:
            percentages=(ny-ny.min())/(ny.max()-ny.min())*100
        except TypeError as te:
            print("Can't get percentages for ",j)
            all_jobs.remove(j)
            # raise te
        # print(percentages)
        for p,_y in zip(pkeys,percentages):
            rpipelines[p][j]=_y
    return rpipelines

# Cleanup idea from: https://stackoverflow.com/a/16562028
def cleanup(npipelines, tolerance=10):
    """requires all_jobs (i.e. normalized_jobs)
    use median and standard deviation median to determine
    ratio of data point deviation and compare it to tolerance
    """
    rpipelines=deepcopy(npipelines)
    
    for j in all_jobs:
        y=[]
        pkeys=list(rpipelines.keys())
        # first pass: collect job's runtime
        for p in pkeys:
            jd=rpipelines[p][j]
            if jd is None:
                y.append(numpy.nan)
            else:
                y.append(jd)
        # second pass: denoise values  
        ny=numpy.array(y)
        try:
            median=numpy.nanmedian(ny)
        except TypeError as te:
            print("Failed to median: ", j, ny)
            continue
        nyd=numpy.abs(ny-median)
        mdev=numpy.nanmedian(nyd)
        # print(median, mdev, nyd)
        # third pass: re-inject values back
        for p in pkeys:
            d=rpipelines[p][j] 
            if d is None:
                # No value
                continue
            rel_dev=(abs(d-median))/mdev
            if rel_dev > tolerance:
                print("Deviation spotted (and removed): ", j, d, rel_dev)
                del rpipelines[p]
    return rpipelines

def cleanup_full(npipelines, tolerance=10):
    """requires all_jobs (i.e. normalized_jobs)"""
    rpipelines=deepcopy(npipelines)
    
    for j in all_jobs:
        y=[]
        pkeys=list(rpipelines.keys())
        # first pass: collect job's runtime
        first_value=None
        reduction_needed=False
        for p in pkeys:
            jd=rpipelines[p][j]
            if first_value is None:
                if jd:
                    first_value=jd
                else:
                    reduction_needed=True
            y.append(rpipelines[p][j])
        if reduction_needed:
            print("Reducing NaN values: ", j, end='')
            for i,_y in enumerate(y):
                if _y is None:
                    y[i]=first_value
                    print(".", end='')
                else:
                    print("")
                    break
        # second pass: denoise values  
        ny=numpy.array(y)
        try:
            median=numpy.median(ny)
        except TypeError as te:
            print("Failed to median: ", j, ny)
            continue
        nyd=numpy.abs(ny-median)
        mdev=numpy.median(nyd)
        # print(median, mdev, nyd)
        # third pass: re-inject values back
        for p in pkeys:
            d=rpipelines[p][j] 
            try:
                rel_dev=(abs(d-median))/mdev
            except TypeError as te:
                print("Failed to compute relative deviation: ", j, d, median, mdev)
                continue
            if rel_dev > tolerance:
                print("Deviation spotted (and removed): ", j, d, rel_dev)
                del rpipelines[p]
    return rpipelines

def plot_chunked(npipelines, poly_n=1, values=False, nchunks=1, connect_values=False, suffix='', trend_line=True):
    pipelines=list(npipelines.keys())
    pipelines.sort()
    x_all=[int(i) for i in pipelines]
    x_all.sort()
    xchunks=_split(x_all,nchunks)
    # print("Chunked:", len(x_all), len(xchunks), [len(c) for c in xchunks])
    xp=numpy.linspace(x_all[0],x_all[-1],(x_all[-1]-x_all[0]) % 3)
    for j in all_jobs:
        # print(j, end=' ')
        y=[]
        for _p in x_all:
            y.append(npipelines[str(_p)][j])
        ychunks=_split(y,nchunks)
        try:
            if trend_line:
                plist=[]
                for xc,yc in zip(xchunks,ychunks):
                    # print("Inputs:",xc,yc)
                    # get polynomial coefficients
                    _p=numpy.polyfit(xc,yc,poly_n)
                    # print("Output: ", _p)
                    # print("Polyval:", numpy.polyval(_p,xc))
                    # plist.extend((xc,numpy.polyval(_p,xc),'-'))
                    plist.append(numpy.polyval(_p,xc))
                p=numpy.concatenate(plist)
                # print(numpy.polyval(p,306175), numpy.polyval(p,int(pipelines[-1])), sep=', ')
                # print(x_all,p)
                # plt.plot(*plist,label=j+'_trend')
                plt.plot(x_all,p,label=j+suffix+'_trend')
            if values:
                plt.plot(x_all,y, '-' if connect_values else '.',label=j+suffix)
            plt.legend()
        except Exception as e:
            print(j,"  FAILED")
            print(traceback.format_exc())
            print(e)
            pass
    # plt.show()

def filter_jobs(pipelines, filter=None, filter_out=None):
    """works only on normalized pipelines"""
    if filter is None and filter_out is None:
        return pipelines
    result={}
    for pname in pipelines:
        result[pname]={}
        preserve_jobs=set()
        if filter:
            pjobs=set(pipelines[pname].keys())
            preserve_jobs=set(filter).intersection(pjobs)
        else:
            preserve_jobs=set(pipelines[pname].keys())
        if filter_out:
            for fojob in filter_out:
                try:
                    preserve_jobs.remove(fojob)
                except KeyError:
                    pass
        for j in preserve_jobs:
            try:
                result[pname][j]=pipelines[pname][j]
            except Exception as e:
                print(pipelines[pname])
                raise e
    return result

parser = argparse.ArgumentParser()
parser.add_argument('-O', '--output', help='Output format', choices=['json','csv','none'], default='json')
parser.add_argument('-d', '--denoise-type', help='Denoise type to use', choices=['savgol','lfilter'], default='savgol')
parser.add_argument('--denoise-window', help="Savgol window parameter", type=int, default=55)
parser.add_argument('--denoise-delay', help="lfilter delay param", type=int, default=15)
parser.add_argument('--cleanup-tolerance', help="Cleanup deviation tolerance (higher-more tolerant for outliers)", type=int, default=10)
parser.add_argument('-f', '--output-file', help='Output file', default='-')
parser.add_argument('-p', '--process-pipeline', nargs='+', default=['reduce_pipelines','reduce_to_jobs','normalize_jobs','delta_jobs'])
parser.add_argument('--connect-values', action='store_true', default=False)
parser.add_argument('--plot-values', action='store_true', default=False)
parser.add_argument('--pipeline-duration', action='store_true', default=False)
parser.add_argument('--jobs-total-duration', action='store_true', default=False)
parser.add_argument('--no-trends', dest='trends', action='store_false', default=True)
parser.add_argument('--plot-poly', type=int, default=1)
parser.add_argument('--filter-jobs', nargs='+', default=None)
parser.add_argument('--filter-out-jobs', nargs='+', default=None)
parser.add_argument('--skip-show', action='store_true', default=False)
parser.add_argument('--before',default=None)
parser.add_argument('--after', default=None)
parser.add_argument('-n','--chunks', type=int, default=1)

parser.add_argument('files', nargs='+')
args = parser.parse_args()
all_jobs=set()
all_pipelines=load_files(args.files, args.after, args.before)

dependencies={
    'reduce_pipelines': [],
    'reduce_to_jobs': ['reduce_pipelines'],
    'normalize_jobs': ['reduce_to_jobs'],
    'delta_jobs': ['normalize_jobs'],
    'plot': ['reduce_to_jobs']
}

## command_stack=[]
## def expand_process_pipeline(ppipelines):
##     resolved=[]
##     for p in ppipelines:
##         if p in command_stack:
##             continue
##         if dependencies[p]:
##             resolved.expand(expand_process_pipeline(dependencies[p]))
##         resolved.append(p)
##     return resolved

current_pipelines=all_pipelines

plot_show=False
suffix=""
for pp in args.process_pipeline:
    if pp == 'reduce_pipelines':
        new_pipelines=reduce_pipelines(current_pipelines)
        current_pipelines=new_pipelines
        suffix="_reduced"
    elif pp == 'filter_jobs':
        new_pipelines = filter_jobs(current_pipelines, args.filter_jobs, args.filter_out_jobs)
        current_pipelines=new_pipelines
        suffix="_filtered"
    elif pp == 'reduce_to_jobs':
        new_pipelines=reduce_to_jobs(current_pipelines, args.pipeline_duration, args.jobs_total_duration)
        current_pipelines=new_pipelines
        suffix="_reduce_jobs"
    elif pp == 'normalize_jobs':
        new_pipelines, all_jobs = normalize_jobs(current_pipelines)
        current_pipelines=new_pipelines
        suffix="_normalized"
        print(all_jobs)
    elif pp == 'delta_jobs':
        new_pipelines = extract_delta(current_pipelines)
        current_pipelines=new_pipelines
        suffix="_delta"
    elif pp == 'cleanup':
        new_pipelines = cleanup(current_pipelines, args.cleanup_tolerance)
        current_pipelines=new_pipelines
        suffix="_cleaned"
    elif pp == 'percentage':
        new_pipelines = to_percentage(current_pipelines)
        current_pipelines=new_pipelines
        suffix="_perecentage"
    elif pp == 'denoise':
        if args.denoise_type == 'savgol':
            new_pipelines = denoise_savgol(current_pipelines, args.denoise_window)
        elif args.denoise_type == 'lfilter':
            new_pipelines = denoise_lfilter(current_pipelines, args.denoise_delay)
        current_pipelines=new_pipelines
        suffix="_denoised"
    elif pp == 'plot':
        plot(current_pipelines, 
             poly_n=args.plot_poly, 
             values=args.plot_values, 
             trend_line=args.trends,
             connect_values=args.connect_values, 
             suffix=suffix)
        plot_show=True
    elif pp == 'nplot':
        plot_chunked(current_pipelines, 
                     poly_n=args.plot_poly, 
                     values=args.plot_values, 
                     trend_line=args.trends,
                     nchunks=args.chunks, 
                     connect_values=args.connect_values, 
                     suffix=suffix)
        plot_show=True
    elif pp == 'save':
        # plt.grid(visible=True, alpha=0.5)
        plt.savefig('myfig.png', format='png', dpi=600, bbox_inches="tight", transparent= True)

# reduced_pipelines=reduce_pipelines(all_pipelines)
# reduced_to_jobs=reduce_to_jobs(reduced_pipelines)
# normalized_jobs, all_jobs=normalize_jobs(reduced_to_jobs)
# delta_jobs=extract_delta(normalized_jobs)

# plot(normalized_jobs)
if plot_show and not args.skip_show:
    plt.show()

if args.output == 'json':
    print(json.dumps(current_pipelines, indent=2))
elif args.output == 'csv':
    f=sys.stdout
    if args.output_file != '-':
        f=open(args.output_file,'w')
    writer=DictWriter(f,fieldnames=['pipeline']+list(all_jobs))
    writer.writeheader()
    for pname in current_pipelines:
        writer.writerow({'pipeline': pname} | current_pipelines[pname])
    f.close()
