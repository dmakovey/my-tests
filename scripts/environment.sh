#!/bin/bash

GLAB=${GLAB:-"glab"}
# GL_FLOW=${GL_FLOW:-${HOME}/work/python/gl-workflow/scripts/gl-flow.py}
PROJECT_ID=${PROJECT_ID:-3828396}
ENVIRONMENTS_FILE=${ENVIRONMENTS_FILE:-${HOME}/tmp/${PROJECT_ID}_environments.json}
MAX_AGE=${MAX_AGE:-1}

get_environments(){
    if [[ -r "${ENVIRONMENTS_FILE}" ]]; then
        cat "${ENVIRONMENTS_FILE}"
    else
        query_environments_file
    fi
}

query_environments_file(){
    if [[ -n "${GL_FLOW}" ]]; then
        ${GL_FLOW} environments --format=brief --state=available ${PROJECT_ID} | tee ${ENVIRONMENTS_FILE}
    else
        ${GLAB} api "projects/${PROJECT_ID}/environments?states=available" | tee ${ENVIRONMENTS_FILE}
    fi
}


filter_old_environments(){
    jq --argjson max_age "${MAX_AGE}" '[.[] | now as $now  | {"name": .name, "id": .id, "created": (.created_at | strptime("%Y-%m-%dT%H:%M:%S.%GZ") | mktime), "updated": (.updated_at | strptime("%Y-%m-%dT%H:%M:%S.%GZ") | mktime)} | select($now-.updated > $max_age*60*60*24) | select(.name | contains("production")|not)]'
}

list_old_environments(){
    get_environments | filter_old_environments | jq -r '.[] | "\(.id)|\(.name)"'
}

stop(){
    # for env_id in $(get_environments | filter_old_environments | jq -r '.[] | .id')
    for env_str in $(list_old_environments)
    do
        env_id=${env_str%%|*}
        env_name=${env_str#*|}
        echo "# Environment: ${env_name}"  | tee _data/${env_id}.sh
        if [[ -n "${GL_FLOW}" ]]; then
            echo "${GL_FLOW} api --method post /projects/${PROJECT_ID}/environments/${env_id}/stop 2>&1 | tee _data/${env_id}.json" | tee -a _data/${env_id}.sh
        else
            echo "${GLAB} api --method POST projects/${PROJECT_ID}/environments/${env_id}/stop 2>&1 | tee _data/${env_id}.json" | tee -a _data/${env_id}.sh
        fi
    done
}

purge(){
    rm ${ENVIRONMENTS_FILE}
}

if [[ $# -lt 1 ]]; then
    list_old_environments
else
    for cmd in "$@"
    do
        $cmd
    done
fi