#!/bin/bash -x

# Create service-specific TLS cert & corresponding secret

TLS_KEY=${TLS_KEY:-"tls.key"}
TLS_CERT=${TLS_CERT:-"tls.crt"}
NAMESPACE=${NAMESPACE:-"default"}
SERVICE_NAME=${SERVICE_NAME:-"gitlab-webservice-default"}
SERVICE_HOST=${SERVICE_HOST:-"${SERVICE_NAME}.${NAMESPACE}.svc.cluster.local"}
TLS_SECRET_NAME=${TLS_SECRET_NAME:-"gitlab-webservice-secret"}

create_cert(){
    openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
        -keyout "${TLS_KEY}" -out "${TLS_CERT}" \
        -subj "/CN=${TLS_HOST}/O=${TLS_HOST}"
}

create_secret(){
    kubectl create secret tls -n ${NAMESPACE} ${TLS_SECRET_NAME} --cert=${TLS_CERT} --key=${TLS_KEY}
}

for cmd in "$@"
do 
    ${cmd}
done