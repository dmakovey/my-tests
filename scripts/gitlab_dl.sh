#!/bin/sh

# Very crude way of downloading artifacts locally based on URL provided in WebUI


# This is how we *should* download
# curl --location --output artifacts.zip --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.example.com/api/v4/projects/1/jobs/42/artifacts"
#
# This is what WebUI gives us:
# URI=https://dev.gitlab.org/gitlab/omnibus-gitlab/-/jobs/15167080/artifacts/download

URI=${1}

URI_PATH=${URI#http*://*/}
URI_BASE=${URI%/${URI_PATH}}
PROJECT_PATH=${URI_PATH%%/-/*}
PROJECT_NAME=${PROJECT_PATH##*/}
PROJECT_PATH_ENCODED=$(echo "${PROJECT_PATH}"| jq -Rr @uri)

# Get numerical PROJECT_ID
# PROJECT_ID=$(curl --location --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}"  "${URI_BASE}/api/v4/projects/${PROJECT_PATH_ENCODED}" | jq .id)

JOB_SECTION=${URI_PATH##*/-/}
ARTIFACT_SUFFIX=${JOB_SECTION%/download}

ARTIFACT_URI="${URI_BASE}/api/v4/projects/${PROJECT_PATH_ENCODED}/${ARTIFACT_SUFFIX}"

curl \
  --location \
  --output "${PROJECT_NAME}-artifact".zip \
  --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
  "${ARTIFACT_URI}"

