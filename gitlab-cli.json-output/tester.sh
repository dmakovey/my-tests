#!/bin/sh -x

PROJECT=${PROJECT:-"OWNER/REPO"}
MITM_PORT=${MITM_PORT:-8082}
MITM_WRAPPER=${MITM_WRAPPER:-"mitm_wrapper.py"}
GLAB=${GLAB:-bin/glab}
OUTPUT_DIR=${OUTPUT_DIR:-"."}

TEST_NAME=${TEST_NAME:-"$@"}

OUTPUT_DIR=$(realpath ${OUTPUT_DIR})

session_name=${1}_${2}

_my_path=$(realpath $0)
_my_dir=$(dirname ${_my_path})

mkdir -p ${OUTPUT_DIR}

command_shortener(){
    shift 2
    echo "$@"
}

cmd=""
case "${session_name}" in
    "project_view")
        cmd="$@ ${PROJECT}"
        ;;
    "project_list")
        cmd="$@"
        ;;
    *)
        cmd="-R ${PROJECT} $@"
        ;;
esac

mitmdump -q -w ${OUTPUT_DIR}/${session_name}.dump -p ${MITM_PORT} -s ${_my_dir}/${MITM_WRAPPER} --set output=${OUTPUT_DIR}/${session_name}.log 2>${OUTPUT_DIR}/${session_name}_error.log &

sleep 3

echo "glab ${cmd}" > ${OUTPUT_DIR}/${session_name}.sh
https_proxy="http://localhost:${MITM_PORT}" ${GLAB} ${cmd} > ${OUTPUT_DIR}/${session_name}_result.json

https_proxy="http://localhost:${MITM_PORT}" curl --proxy localhost:${MITM_PORT} http://shut.down/now

# jq -s . ${OUTPUT_DIR}/${session_name}.log > ${OUTPUT_DIR}/${session_name}.json
jq -s --arg args "${cmd}" --arg name "${TEST_NAME}" '{"name": $name, "args": $args, "http": .}' ${OUTPUT_DIR}/${session_name}.log > ${OUTPUT_DIR}/${session_name}.json