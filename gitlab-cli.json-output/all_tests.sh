#!/bin/bash

set -e

BASE_DATA_DIR=${BASE_DATA_DIR:-data}
RAW_DIR=${RAW_DIR:-"${BASE_DATA_DIR}/raw"}
export OUTPUT_DIR="${RAW_DIR}"
ANON_DIR=${ANON_DIR:-"${BASE_DATA_DIR}/anon"}
export PROJECT="dmakovey/test-vars"
CODE_DIR=${CODE_DIR:-"${BASE_DATA_DIR}/code"}

_my_path=$(realpath $0)
_my_dir=$(dirname ${_my_path})

tests_manifest=${TEST_MANIFEST:-${_my_dir}/all_tests.json}

dump(){
  for i in $(seq 0 $(jq '. | length - 1' ${tests_manifest}))
  do
    cmd=$(jq -r ".[$i].command" ${tests_manifest})
    name=$(jq -r ".[$i].name" ${tests_manifest})
    TEST_NAME="${name}" ${_my_dir}/tester.sh $cmd
  done
}

anonymize(){
  rm -rf "${ANON_DIR}"

  cp -r "${OUTPUT_DIR}" "${ANON_DIR}"

  pushd ${ANON_DIR}

  for i in *.json *.sh
  do
    sed -i 's/dmakovey/OWNER/g; s/Dmytro Makovey/Some User/g; s/test-vars/REPO/g' ${i}
  done

  popd 
}

generate(){
  mkdir -p ${CODE_DIR}

  for i in ${ANON_DIR}/*.sh 
  do
    # cmd_dir=$(awk '{print $2,"/",$3;}' ${i})
    _basename=$(basename $i)
    cmd_dir=$(echo $_basename | sed 's#^\(.*\)_\(.*\)\.sh$#\1/\2#g')
    dst_dir=${CODE_DIR}/${cmd_dir}/testdata
    mkdir -p ${dst_dir}
    cmd_id=${_basename%.sh}
    for fnum in $(seq 0 $(jq '.http | length - 1 ' ${ANON_DIR}/${cmd_id}.json))
    do
      # jq --arg fnum "${fnum}" '.http[$fnum|tonumber].request' "${ANON_DIR}/${cmd_id}".json > "${dst_dir}/${cmd_id}-${fnum}.request"
      jq --arg fnum "${fnum}" '.http[$fnum|tonumber].response.body' "${ANON_DIR}/${cmd_id}".json > "${dst_dir}/${cmd_id}-${fnum}.json"
    done
    cp ${ANON_DIR}/${cmd_id}_result.json ${dst_dir}/${cmd_id}.result
    # sed -e 's/^glab //g' ${i} > ${dst_dir}/${cmd_id}.cmd
    cat ${_my_dir}/go_test.tmpl | COMMAND_NAME="testdata/${cmd_id}" gomplate --datasource test_data=file://$(realpath "${ANON_DIR}/${cmd_id}".json) --datasource testfile=env:COMMAND_NAME > "${CODE_DIR}/${cmd_dir}/${cmd_id}_testme.inc"
  done
}

if [ $# -lt 1 ]; then
  dump
  anonymize
  generate 
else
  for cmd in "$@"
  do
    $cmd
  done
fi