## Intro

Generate test data for GitLab CLI JSON output.

## Sources of information (inputs)

* Tests are based on manifest in `all_tests.json` - outlining name of the test and command to execute the test.
* Default project used for tests generation is `dmakovey/test-vars`

## Prerequisites

1. Python
1. MITM proxy is needed to capture requests and responses to GitLab API:
   ```shell
   pip install mitmproxy
   ```
1. `jq`
1. locally compiled `glab`

## Setup and Execution

1. clone both `my-tests` and `gitlab-org/cli` repos
   ```shell
   git clone git@gitlab.com:dmakovey/my-tests.git 
   git clone git@gitlab.com:gitlab-org/cli.git gitlab-cli
   ```
1. (optional) for convenience create a symlink:
   ```shell
   ln -s ${PWD}/my-tests/gitlab-cli.json-output gitlab-cli/scripts/tests-generator
   ```
1. Execute from `gitlab-cli` root folder:
   ```shell
   bash -x scripts/tests-generator/all_tests.sh
   ```
1. Directory will be populated:
   * `raw`  - actual data from sent and recieved with GitLab API
   * `anon` - anonymized data
   * `code` - rough layout for test data drop-in locations in code along with corresponding data (at present incomplete)
