"""
A simple way of shutting down the mitmproxy instance to stop everything.

Usage:

    mitmproxy -s shutdown.py

    and then send a HTTP request to trigger the shutdown:
    curl --proxy localhost:8080 http://example.com/path
"""
import logging

from mitmproxy import ctx, http
# from mitmproxy.http import Headers
import json

class SetOutput:
    def __init__(self):
        self.num = 0

    def load(self, loader):
        loader.add_option(
            name="output",
            typespec=str,
            default="out.json",
            help="Output to file",
        )
        

addons = [SetOutput()]

def request(flow: http.HTTPFlow):
    if flow.request.pretty_url == "http://shut.down/now":
        logging.info("Shutting down everything...")
        ctx.master.shutdown()
    # print(flow.request)


def response(flow: http.HTTPFlow):
    res = {
            "request": {
                "url": flow.request.path, 
                "method": flow.request.method
            },
            "response": {
                "body": flow.response.json(),
                "status": flow.response.status_code
            }}
    with open(ctx.options.output, 'a') as f:
        print(json.dumps(res), file=f)
