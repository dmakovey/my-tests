#!/bin/bash

function _print_image_info(){
    FROM_REF=${1}

    _has_sha=false
    _has_tag=false
    if echo ${FROM_REF} | grep -qF '@'; then
        _has_sha=true
        if echo ${FROM_REF} | grep -qe ':.*:'; then
            _has_tag=true
        else
            _has_tag=false
        fi
    else
        _has_sha=false
        if echo ${FROM_REF} | grep -F ':'; then
            _has_tag=true
        fi
    fi

    if [ "${_has_sha}" = "true" ]; then
        if [ "${_has_tag}" = "true" ]; then
            FROM_IMG=${FROM_REF%%:*}
        else
            FROM_IMG=${FROM_REF%%@*}
        fi
        FROM_SHA=${FROM_IMG}@${FROM_REF#*@}
    else
        FROM_SHA=""
    fi
    if [ "${_has_tag}" = "true" ]; then
        FROM_TAG=${FROM_REF%%@*}
        FROM_IMG=${FROM_REF%%:*}
    else
        FROM_TAG=""
    fi

    echo "> Base image: ${FROM_IMG}"
    for img in ${FROM_SHA} ${FROM_TAG}
    do
        echo "==> Checking $img"
        echo -n "-> mediaType: "
        skopeo inspect --raw docker://${img} | jq .mediaType

        echo "-> Labels"
        skopeo inspect docker://${img} | jq .Labels
    done
}

for i in "$@"
do
    _print_image_info "${i}"
done
