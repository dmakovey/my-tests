#!/bin/bash

PROJECT_ID=${PROJECT_ID:-4359271}
MR_ID=${MR_ID:-1425}
REF=${REF:-"refs/merge-requests/${MR_ID}/merge"}
DATA_DIR=${DATA_DIR:-_data}
timespamp=$(date '+%Y%m%d%H%M%S')
SESSION=${SESSION:-pipeline-${timespamp}}
INCLUDE_RETRIED=${INCLUDE_RETRIED:-"true"}
SOURCE=${SOURCE:-""}

date_filtering=${FROM_DATE:+"true"}

FROM_DATE=${FROM_DATE:-"2 weeks ago"}
date_str=$(date --iso-8601=minutes --date="2 weeks ago")}

mkdir -p ${DATA_DIR}

report() {
	for j in ${DATA_DIR}/*-jobs.json; do
		jq -r ' .[] | select(.duration > 60*60) | { "name": .name, "pipeline": .pipeline.web_url, "job": .web_url} | "* [\( .name )](\(.job)) in \(.pipeline)"' ${j}
	done
}

stats() {
	for j in ${DATA_DIR}/*-jobs.json; do
		jq -r '[.[] | select(.duration > 60*60) | { "name": .name, "pipeline": .pipeline.web_url, "job": .web_url}]' ${j}
	done | jq -rn '[inputs|.[]] | group_by(.name) | .[] | "\(. | length) \(.[0].name)"' | sort -n -k 1
}

references() {
	for j in ${DATA_DIR}/*-jobs.json; do
		jq -r '.[] | select(.duration > 60*60) | "* [\(.name)](\(.web_url)) from [\(.pipeline.id)](\(.pipeline.web_url))"' ${j}
	done | sort
}

collect() {
	local date_field=""
	local source_field=""
	if [[ "$date_filtering" == "true" ]]; then
		date_field="-F updated_after=\"${date_str}\""
	fi
	if [[ -n "${SOURCE}" ]]; then
		source_field="-F source=${SOURCE}"
	fi

	glab api --paginate -X GET -F ref=${REF} ${date_field} ${source_field} /projects/${PROJECT_ID}/pipelines >${DATA_DIR}/pipelines.jsonn
	# Normalize JSON stream into single JSON object
	for _pid in $(jq -rn '[inputs|.[]]| .[] | .id' ${DATA_DIR}/pipelines.jsonn); do
		glab api --paginate -X GET -F include_retried=${INCLUDE_RETRIED} /projects/${PROJECT_ID}/pipelines/${_pid}/jobs >${DATA_DIR}/${_pid}-jobs.json
	done
}

if [ $# -lt 1 ]; then
	echo "Usage: $0 [collect|stats|report]"
else
	for cmd in "$@"; do
		$cmd
	done
fi
