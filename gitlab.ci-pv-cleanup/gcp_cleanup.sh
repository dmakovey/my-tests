#/bin/env bash

GCLOUD_PROJECT=cloud-native-182609

gcloud compute disks list --project ${GCLOUD_PROJECT}\
	--format=json --filter="-users:*" \
	| jq -r --arg project "${GCLOUD_PROJECT}" '.[] | select (.lastDetachTimestamp != null and .lastAttachTimestamp != null and .lastAttachTimestamp < .lastDetachTimestamp and .labels["goog-k8s-cluster-location"] != null) | "gcloud compute disks delete --project \($project) --zone \(.labels["goog-k8s-cluster-location"]) -q \(.name)"'
