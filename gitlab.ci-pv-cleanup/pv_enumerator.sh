#!/bin/bash

function _k() {
	kubectl --context=${KCTX} $@
}

for KCTX in cloud-native-v12{2,5,6}; do
	pvs=$(
		_k get pvc --no-headers --selector 'app.kubernetes.io/instance=rvw-ci-pv-cleanup' -o json | jq -r '.items[].spec.volumeName'
		_k get pvc --no-headers --selector 'release=rvw-ci-pv-cleanup' -o json | jq -r '.items[].spec.volumeName'
	)
	echo "==> $KCTX"
	for pv in $pvs; do
		vh=$(_k get pv "$pv" -o json | jq -r '.spec.csi.volumeHandle')
		status=$(gcloud compute disks describe --zone=us-central1-b --format json $vh | jq -r '.status')
		echo "$pv $vh $status" >&2
	done
done
