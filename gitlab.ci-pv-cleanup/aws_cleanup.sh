#!/bin/bash
#
# collect data
aws ec2 describe-instances > _data/aws-instances.json
aws ec2 describe-volumes > _data/aws-volumes.json
aws ec2 describe-volumes --filters "Name=status,Values=available" \
  > _data/aws-volumes-available.json

# create cleanup script
jq -r '.Volumes[].VolumeId | "aws ec2 delete-volume --volume-id \(.)"' \
  _data/aws-volumes-available.json \
  > _data/aws-cleanup.sh
