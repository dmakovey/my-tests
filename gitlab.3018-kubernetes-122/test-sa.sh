#!/bin/bash

CLUSTER_DIR=${CLUSTER_DIR:-"cloud-native-v122"}
CLUSTER_NAME=${CLUSTER_NAME:-"cloud-native-v122"}
GKE_PROJECT=${GKE_PROJECT:-"cloud-native-182609"}
CLUSTER_ZONE=${CLUSTER_ZONE:-"us-cerntral1-b"}

CONTEXT_BASENAME=${CONTEXT_BASENAME:-"sa-user"}
CONTEXT_NAME=${CONTEXT_NAME:-${CONTEXT_BASENAME}}
CONTEXT_CLUSTER_NAME=${CONTEXT_CLUSTER_NAME:-${CONTEXT_BASENAME}}
CONTEXT_USER_NAME=${CONTEXT_USER_NAME:-${CONTEXT_BASENAME}}

CI_NAMESPACE=${CI_NAMESPACE:-"helm-charts-win"}

## Mock CI vars
CI_COMMIT_TITLE=${CI_COMMIT_TITLE:-"foobar"}
CI_COMMIT_SHA=${CI_COMMIT_SHA:-"a1b2c3d4"}
CI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME:-"3018-kubernetes-122"}
CI_JOB_URL=${CI_JOB_URL:-"https://foo.bar/job"}
CI_PIPELINE_URL=${CI_PIPELINE_URL:-"https://foo.bar/pipeline"}
CI_ENVIRONMENT_SLUG=${CI_ENVIRONMENT_SLUG:-"foo-environment"}

gcloud_auth(){
    gcloud container clusters get-credentials ${CLUSTER_NAME} --zone ${CLUSTER_ZONE} --project ${GKE_PROJECT}
}

create_gitlab_sa(){

    cat << EOF | kubectl apply -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: gitlab
    namespace: kube-system
EOF

}

get_default_token(){
    kubectl -n kube-system get secret $(kubectl -n kube-system get secret | grep -F default-token | awk '{print $1}') -o jsonpath="{['data']['ca\.crt']}" | base64 -d > ${CLUSTER_DIR}/default-token-ca
}

get_gitlab_token(){
    kubectl -n kube-system get secret $(kubectl -n kube-system get secret | grep gitlab | awk '{print $1}')  -o jsonpath="{['data']['token']}" | base64 --decode > ${CLUSTER_DIR}/gitlab-token
}

get_endpoint(){
    kubectl cluster-info | grep -E 'Kubernetes master|Kubernetes control plane' | awk '/http/ {print $NF}' | sed -e 's/\x1b\[[0-9;]*m//g' > ${CLUSTER_DIR}/endpoint
}

create_namespace(){
  kubectl create namespace ${CI_NAMESPACE}
}

configure_context(){
    local CLUSTER_ENDPOINT=$(cat ${CLUSTER_DIR}/endpoint)
    kubectl config set-cluster ${CONTEXT_CLUSTER_NAME}  --server="${CLUSTER_ENDPOINT}" --certificate-authority="${CLUSTER_DIR}/default-token-ca"
    kubectl config set-credentials ${CONTEXT_USER_NAME} --token=$(cat ${CLUSTER_DIR}/gitlab-token)
    kubectl config set-context ${CONTEXT_NAME} --user ${CONTEXT_USER_NAME} --cluster=${CONTEXT_CLUSTER_NAME} --namespace=${CI_NAMESPACE}
    kubectl config use-context ${CONTEXT_NAME}
}


show_context(){
    kubectl config current-context
}

generate_ci_scale_values(){
  cat << EOSCALE > ${CLUSTER_DIR}/ci.scale.yaml
gitlab:
  webservice:
    minReplicas: 1    # 2
    maxReplicas: 3    # 10
    resources:
      requests:
        cpu: 500m     # 900m
        memory: 1500M # 2.5G
  sidekiq:
    minReplicas: 1    # 1
    maxReplicas: 2    # 10
    resources:
      requests:
        cpu: 500m     # 900m
        memory: 1000M # 2G
  gitlab-shell:
    minReplicas: 1    # 2
    maxReplicas: 2    # 10
  toolbox:
    enabled: true
nginx-ingress:
  controller:
    replicaCount: 1   # 2
redis:
  resources:
    requests:
      cpu: 100m
minio:
  resources:
    requests:
      cpu: 100m
EOSCALE
}

generate_ci_details_values(){
  cat << EODETAILS
ci:
  title: |
    ${CI_COMMIT_TITLE}
  sha: "${CI_COMMIT_SHA}"
  branch: "${CI_COMMIT_REF_NAME}"
  job:
    url: "${CI_JOB_URL}"
  pipeline:
    url: "${CI_PIPELINE_URL}"
  environment: "${CI_ENVIRONMENT_SLUG}"
EODETAILS
}

deploy(){
  ## Requires ci.details.yaml and ci.scale.yaml to be pre-generated
  helm upgrade --namespace=${CI_NAMESPACE} --install --wait --timeout 900s \
    -f ${CLUSTER_DIR}/ci.details.yaml \
    -f ${CLUSTER_DIR}/ci.scale.yaml \
    --set releaseOverride=k8s122-review-301-ai49tg \
    --set global.image.pullPolicy=Always \
    --set global.hosts.hostSuffix=k8s122-review-301-ai49tg-122 \
    --set global.hosts.domain=helm-charts.win \
    --set 'global.ingress.annotations.external-dns\.alpha\.kubernetes\.io/ttl=10' \
    --set global.ingress.tls.secretName=helm-charts-win-tls \
    --set global.ingress.configureCertmanager=false \
    --set global.appConfig.initialDefaults.signupEnabled=false \
    --set nginx-ingress.controller.electionID=k8s122-review-301-ai49tg \
    --set nginx-ingress.controller.ingressClassByName=true \
    --set nginx-ingress.controller.ingressClassResource.controllerValue=ci.gitlab.com/k8s122-review-301-ai49tg \
    --set certmanager.install=false \
    --set prometheus.install=false \
    --set prometheus.server.retention=4d \
    --set global.gitlab.license.secret=k8s122-review-301-ai49tg-gitlab-license \
    --version=600606671-2791464665 \
    k8s122-review-301-ai49tg \
    .
}

all(){
  gcloud_auth
  create_gitlab_sa
  get_default_token
  get_gitlab_token
  get_endpoint
  create_namespace
  show_context
  configure_context
  show_context
  generate_ci_details_values
  generate_ci_scale_values
  deploy
}

for cmd in "$@"
do
    $cmd
done