#!/bin/sh

set -e

latest_k8s_versions(){
    skopeo list-tags docker://kindest/node \
    | jq -r '.Tags | group_by(.|match("v.*\\.*\\.") | .string) | [.[] | map(. | split(".") ) | sort | last | join(".")] | .[]'
}


branch_name(){
    git branch --show-current
}

email(){
    git config user.email
}

K8S_VERSION=${K8S_VERSION:-"v1.24.7"}
LOCAL_IP=${LOCAL_IP:-"127.0.0.1"}
CLUSTER_NAME=${CLUSTER_NAME:-"test"}
KUBECTL=${KUBECTL:-kubectl}
KUBERNETES_TIMEOUT=${KUBERNETES_TIMEOUT:-"300s"}
TARGET_NAMESPACE=${TARGET_NAMESPACE:-"gitlab-system"}
KIND=${KIND:-kind}
GITLAB_CHART_DIR=${GITLAB_CHART_DIR:-"."}

create_k8s(){
    ${KIND} create cluster --retain --image kindest/node:${K8S_VERSION} --name "${CLUSTER_NAME}-${K8S_VERSION}" --config ${GITLAB_CHART_DIR}/examples/kind/kind-no-ssl.yaml
}

deploy_k8s(){
    local _my_branch=$(branch_name)
    local _my_email=$(email)
    helm upgrade \
        --install gitlab gitlab/gitlab \
        --namespace=${TARGET_NAMESPACE} --create-namespace \
        -f ${GITLAB_CHART_DIR}/examples/kind/values-base.yaml -f ${GITLAB_CHART_DIR}/examples/kind/values-no-ssl.yaml \
        --set "global.kubectl.image.tag=${_my_branch}" \
        --set "global.gitlabVersion=${_my_branch}" \
        --set "nginx-ingress.enabled=false" \
        --set "hosts.domain=${LOCAL_IP}.nip.io" \
        --set "certmanager-issuer.email=${_my_email}"
}

test_k8s(){
   local secret1=$(mktemp secret.${K8S_VERSION}-XXXX)
   local secret2=$(mktemp secret.${K8S_VERSION}-XXXX)
   deploy_k8s
   wait_for_gitlab
   kubectl get secret -n ${TARGET_NAMESPACE} gitlab-rails-secret -oyaml | yq .data > $secret1
   timeout 240s stern -n ${TARGET_NAMESPACE} -l release=gitlab,app=gitlab 2>&1 > ${K8S_VERSION}.log &
   KILLME=$!
   deploy_k8s
   sleep 60
   kubectl get secret -n ${TARGET_NAMESPACE} gitlab-rails-secret -oyaml | yq .data > $secret2
   diff $secret1 $secret2 && rm $secret1 $secret2
   kill ${KILLME}
}


_wait_for_resource(){
  local namespace=${1}
  local resource=${2}
  local wait_loop="until $KUBECTL get -n ${namespace} ${resource} ; do echo 'waiting'; sleep 5; done"
  # make sure it's been created
  timeout ${KUBERNETES_TIMEOUT} bash -c "${wait_loop}"
  $KUBECTL wait --for=condition=Available --timeout=${KUBERNETES_TIMEOUT} -n ${namespace} ${resource}
}

wait_for_gitlab(){
  _wait_for_resource ${TARGET_NAMESPACE} deployment/gitlab-webservice-default
}

check_gitlab(){
    local secret_label=$(${KUBECTL} get secret -o yaml -n ${TARGET_NAMESPACE} gitlab-gitlab-initial-root-password | yq eval '.metadata.labels.app')
    if echo "${secret_label}" | grep -q gitlab; then
        echo "ok"
    else
        echo "fail"
    fi
}

destroy_k8s(){
    ${KIND} delete cluster --name "${CLUSTER_NAME}-${K8S_VERSION}"
}

kind_version(){
    ${KIND} version
}


for cmd in "$@"
do 
    $cmd
done
