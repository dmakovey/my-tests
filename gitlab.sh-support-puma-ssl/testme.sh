#!/bin/bash -x

# GITLAB_HOST_DOMAIN=${GITLAB_HOST_DOMAIN:-192.168.3.194.nip.io}
# GITLAB_HOST=${GITLAB_HOST:-"gitlab.${GITLAB_HOST_DOMAIN}"}
# GITLAB_PUMA_HOST=${GITLAB_PUMA_HOST:-"gitlab.${GITLAB_HOST_DOMAIN}"}

PUMA_KEY_FILE=${PUMA_KEY_FILE:-"puma.key"}
PUMA_CERT_FILE=${PUMA_CERT_FILE:-"puma.crt"}
NAMESPACE=${NAMESPACE:-"default"}
PUMA_HOST=${PUMA_HOST:-"gitlab-webservice-default.${NAMESPACE}.svc.cluster.local"}
PUMA_TLS_SECRET_NAME=${PUMA_TLS_SECRET_NAME:-"gitlab-webservice-secret"}

create_cert(){
    openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
        -keyout "${PUMA_KEY_FILE}" -out "${PUMA_CERT_FILE}" \
        -subj "/CN=${PUMA_HOST}/O=${PUMA_HOST}"
}

create_secret(){
    kubectl create secret tls -n ${NAMESPACE} ${PUMA_TLS_SECRET_NAME} --cert=${PUMA_CERT_FILE} --key=${PUMA_KEY_FILE}
}

for cmd in "$@"
do 
    ${cmd}
done